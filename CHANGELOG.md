# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0-alpha.3](https://gitlab.com/koober-sas/database/compare/v1.0.0-alpha.1...v1.0.0-alpha.3) (2021-02-23)


### Bug Fixes

* **lint:** add missing plugin syntax jsx ([8f2522d](https://gitlab.com/koober-sas/database/commit/8f2522d860af2b9ad2e00a42f46ff5ca38b5dd9a))
* **package:** remove conflictuous package ([836f994](https://gitlab.com/koober-sas/database/commit/836f9945396cf9fb732f8db9f8c948043ce10dfa))





# 1.0.0-alpha.1 (2020-08-31)


### Bug Fixes

* **ci:** correct lerna configuration to work on gitlab ci ([a8e5dea](https://gitlab.com/koober-sas/database/commit/a8e5dea552cabd07a1615dd4692ff150e41e37cb))
* **cspell:** ignore changelog to avoid breaking release script ([6272357](https://gitlab.com/koober-sas/database/commit/6272357c3b7abe059bef251d8fc9b18cb421347c))
