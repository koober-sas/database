# Documentation

## Setup Renovate

1. Open the project on gitlab website
2. Go to CI/CD > Schedules
3. Configure job
    * Set `Interval pattern` to `0 * * * *`
    * Add variables `RENOVATE` to `ON`
