import { SQL, executeQuery, Environment, createEnvironment } from '@koober/database-client';

interface User {
  id: number;
  name: string;
}

export async function getUserById(environment: Environment, id: number): Promise<User | null> {
  const sqlStatement = SQL`SELECT id, name FROM user WHERE id=${id}`;
  const rows = await executeQuery(environment, sqlStatement);

  return Array.isArray(rows) ? rows[0] : undefined;
}

export async function main(): Promise<void> {
  const environment = createEnvironment({
    adapter: 'mysql',
    database: '',
    user: '',
  });

  const response = await getUserById(environment, 123);
  console.log(response);
}
