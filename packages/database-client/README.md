<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=# Plop Documentation plugin _(${name})_) -->
# Plop Documentation plugin _(@koober/database-client)_
<!-- AUTO-GENERATED-CONTENT:END -->

[![NPM Version][package-version-svg]][package-url]
[![License][license-image]][license-url]

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=> ${description}&unknownTxt= ) -->
> Database client library
<!-- AUTO-GENERATED-CONTENT:END -->

## Installation

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=```console\nnpm install --save-dev ${name}\n```) -->
```console
npm install --save-dev @koober/database-client
```
<!-- AUTO-GENERATED-CONTENT:END -->

## Usage

<!-- AUTO-GENERATED-CONTENT:START (CODE:src=./doc/usage.ts) -->
<!-- The below code snippet is automatically added from ./doc/usage.ts -->
```ts
import { SQL, executeQuery, Environment, createEnvironment } from '@koober/database-client';

interface User {
  id: number;
  name: string;
}

export async function getUserById(environment: Environment, id: number): Promise<User | null> {
  const sqlStatement = SQL`SELECT id, name FROM user WHERE id=${id}`;
  const rows = await executeQuery(environment, sqlStatement);

  return Array.isArray(rows) ? rows[0] : undefined;
}

export async function main(): Promise<void> {
  const environment = createEnvironment({
    adapter: 'mysql',
    database: '',
    user: '',
  });

  const response = await getUserById(environment, 123);
  console.log(response);
}
```
<!-- AUTO-GENERATED-CONTENT:END -->

## License
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[${license}][license-url] © ${author}) -->
[MIT][license-url] © Julien Polo <julien.polo@koober.com>
<!-- AUTO-GENERATED-CONTENT:END -->

<!-- VARIABLES -->

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-version-svg]: https://img.shields.io/npm/v/${name}.svg?style=flat-square) -->
[package-version-svg]: https://img.shields.io/npm/v/@koober/database-client.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-url]: https://www.npmjs.com/package/${name}) -->
[package-url]: https://www.npmjs.com/package/@koober/database-client
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[license-image]: https://img.shields.io/badge/license-${license}-green.svg?style=flat-square) -->
[license-image]: https://img.shields.io/badge/license-MIT-green.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
[license-url]: ../../LICENSE
