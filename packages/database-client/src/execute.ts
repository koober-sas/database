import { Result } from '@koober/std/lib/result';
import { Environment } from './environment';
import { SQLStatement, isSQLStatement } from './sql';
import { SQLQuery } from './query';

/**
 * Execute the `sqlStatement` on an `environment`
 *
 * @example
 * const result = await executeQuery(environment, SQLQuery.CreateTable({ tableName: 'test_table' }));
 * if (Result.isSuccess(result)) {
 *   console.log(result.value)
 * } else {
 *   console.error(result.error);
 * }
 *
 * @param environment created with a database adapter `createEnvironment(environmentConfig)` function
 * @param sqlOrQuery SQL query object or a raw sql statement
 */
export async function executeQuery(
  environment: Environment,
  sqlOrQuery: SQLStatement | SQLQuery
): Promise<Result<unknown, unknown>> {
  const sqlStatement = isSQLStatement(sqlOrQuery) ? sqlOrQuery : environment.queryToStatement(sqlOrQuery);

  try {
    const response = await environment.executeQuery(sqlStatement);

    return Result.Success(response);
  } catch (error) {
    return Result.Failure(error);
  }
}
