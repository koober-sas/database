import { SQLStatement } from './sql';
import { SQLQuery } from './query';

/**
 * The environment represents the way to connect to a database server
 */
export interface Environment {
  adapter: string;
  queryToStatement(query: SQLQuery): SQLStatement;
  executeQuery(sqlStatement: SQLStatement): Promise<unknown>;
}
