import { SQLStatement } from 'sql-template-strings';

export { SQL, SQLStatement } from 'sql-template-strings';

/**
 * Return true if anyValue is a SQLStatement
 *
 * @param anyValue
 */
export function isSQLStatement(anyValue: unknown): anyValue is SQLStatement {
  return typeof anyValue === 'object' && anyValue != null
    ? // eslint-disable-next-line @typescript-eslint/no-explicit-any
      Array.isArray((anyValue as Record<string, any>).strings)
    : false;
}
