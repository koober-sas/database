import { Database } from 'sqlite3';
import { SQLStatement } from '../sql';
import { Environment } from '../environment';
import { SQLQuery } from '../query';

export function SQLite3Environment(config: SQLite3Environment.Config): SQLite3Environment.Module {
  const adapter = 'sqlite3';

  const queryToStatement = SQLQuery.toSQLStatement;

  function sqlite3SQLStatement({ sql, ...other }: SQLStatement): SQLStatement {
    return {
      ...other,
      sql: sql.replace(/UNIX_TIMESTAMP\(\)/, "strftime('%s','now')"),
    } as SQLStatement;
  }

  async function executeQuery(sqlStatement: SQLStatement): Promise<unknown> {
    const sqliteStatement = sqlite3SQLStatement(sqlStatement);
    const database = new Database(config.filename);
    const queryResultPromise = new Promise((resolve, reject) => {
      database.all(sqliteStatement.sql, sqliteStatement.values, (error, result) =>
        error ? reject(error) : resolve(result)
      );
    });

    try {
      const queryResult = await queryResultPromise;

      return queryResult;
    } finally {
      database.close();
    }
  }

  return {
    adapter,
    executeQuery,
    queryToStatement,
  };
}

export namespace SQLite3Environment {
  export type Module = Environment & {
    adapter: 'sqlite3';
  };

  export interface Config {
    filename: string;
  }
}
