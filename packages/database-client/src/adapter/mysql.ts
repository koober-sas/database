import { createConnection, ConnectionConfig } from 'mysql';
import { SQLStatement } from '../sql';
import { Environment } from '../environment';
import { SQLQuery } from '../query';

export function MySQLEnvironment(config: MySQLEnvironment.Config): MySQLEnvironment.Module {
  const adapter = 'mysql';

  const queryToStatement = SQLQuery.toSQLStatement;

  async function executeQuery(sqlStatement: SQLStatement): Promise<unknown> {
    const connection = createConnection(config);

    try {
      connection.connect();
      const queryResultPromise = new Promise((resolve, reject) => {
        connection.query(sqlStatement, (error, result) => (error ? reject(error) : resolve(result)));
      });

      const queryResult = await queryResultPromise;

      return queryResult;
    } finally {
      connection.end();
    }
  }

  return {
    adapter,
    executeQuery,
    queryToStatement,
  };
}

export namespace MySQLEnvironment {
  export type Module = Environment & {
    adapter: 'mysql';
  };

  export type Config = ConnectionConfig;
}
