/* eslint-disable promise/prefer-await-to-callbacks */
import * as mysql from 'mysql';
import { SQL } from '..';
import { MySQLEnvironment } from './mysql';

describe('mysql.MySQLEnvironment()', () => {
  const name = 'Toto';
  const anyStatement = SQL`SELECT author FROM books WHERE name=${name}`;
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const mockConnection = (connectionProperties = {}) => {
    jest.spyOn(mysql, 'createConnection').mockImplementation(
      () =>
        ({
          query: jest.fn((_, callback) => callback(null, [])),
          connect: jest.fn(),
          end: jest.fn(),
          ...connectionProperties,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        } as any)
    );
  };
  const anyEnvironment = MySQLEnvironment({
    host: 'foo.com',
  });

  describe('.adapter', () => {
    test('should be "mysql"', () => {
      expect(MySQLEnvironment({}).adapter).toEqual('mysql');
    });
  });

  describe('.executeQuery()', () => {
    test('should open connection', async () => {
      mockConnection();
      const config = {
        host: 'foo.com',
      };
      const environment = MySQLEnvironment(config);
      await environment.executeQuery(anyStatement);

      expect(mysql.createConnection).toHaveBeenLastCalledWith(config);
    });
    test('should send query to connection', async () => {
      const query = jest
        .fn()
        .mockImplementation((queryObject: mysql.Query, callback: (error: Error | null, result: number) => void) => {
          callback(null, 2);
        });
      mockConnection({
        query,
      });

      await anyEnvironment.executeQuery(anyStatement);

      expect(query).toHaveBeenLastCalledWith(anyStatement, expect.any(Function));
    });

    test('should close connection', async () => {
      const end = jest.fn();
      mockConnection({
        end,
      });

      await anyEnvironment.executeQuery(anyStatement);
      expect(end).toHaveBeenCalledTimes(1);
    });

    test('should close connection when callback error', async () => {
      const end = jest.fn();
      const query = jest.fn(
        (queryObject: mysql.Query, callback: (error: Error | null, result: number | null) => void) => {
          callback(new Error('mock error'), null);
        }
      );
      mockConnection({
        end,
        query,
      });

      await expect(anyEnvironment.executeQuery(anyStatement)).rejects.toThrow();
      expect(end).toHaveBeenCalledTimes(1);
    });
  });
});
