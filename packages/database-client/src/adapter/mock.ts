import { SQLStatement } from '../sql';
import { Environment } from '../environment';
import { SQLQuery } from '../query';

export function MockEnvironment({
  mockImplementation = () => Promise.reject(new Error('NotImplementedError')),
}: MockEnvironment.Config): MockEnvironment.Module {
  const adapter = 'mock';

  return {
    adapter,
    executeQuery: mockImplementation,
    queryToStatement: SQLQuery.toSQLStatement,
  };
}

export namespace MockEnvironment {
  export type Module = Environment & {
    adapter: 'mock';
  };

  export interface Config {
    mockImplementation?: (sqlStatement: SQLStatement) => Promise<unknown>;
  }
}
