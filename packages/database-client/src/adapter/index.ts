/* eslint-disable global-require, @typescript-eslint/no-var-requires */
import { Environment } from '../environment';
import { MySQLEnvironment } from './mysql';
import { SQLite3Environment } from './sqlite3';
import { MockEnvironment } from './mock';

export interface MySQLEnvironmentConfig extends MySQLEnvironment.Config {
  adapter: 'mysql';
}

export interface SQLite3EnvironmentConfig extends SQLite3Environment.Config {
  adapter: 'sqlite3';
}

export interface MockEnvironmentConfig extends MockEnvironment.Config {
  adapter: 'mock';
}

export type EnvironmentConfig = MySQLEnvironmentConfig | SQLite3EnvironmentConfig | MockEnvironmentConfig;

/**
 * Create an environment from configuration
 *
 * @param config
 */
export function createEnvironment(config: EnvironmentConfig): Environment {
  switch (config.adapter) {
    case 'mysql':
      return require('./mysql').MySQLEnvironment(config);
    case 'sqlite3':
      return require('./sqlite3').SQLite3Environment(config);
    case 'mock':
      return require('./mock').MockEnvironment(config);
    default:
      throw new Error('AdapterError');
  }
}
