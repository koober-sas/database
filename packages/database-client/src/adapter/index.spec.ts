import { createEnvironment } from '.';

jest.mock('sqlite3', () => ({
  Database: (() => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    function MockDatabase() {
      return undefined;
    }

    return MockDatabase;
  })(),
}));

describe('createEnvironment()', () => {
  test('should return environment when adapter is "mysql"', () => {
    expect(createEnvironment({ adapter: 'mysql' })).toEqual({
      adapter: 'mysql',
      executeQuery: expect.any(Function),
      queryToStatement: expect.any(Function),
    });
  });
  test('should return environment when adapter is "sqlite3"', () => {
    expect(createEnvironment({ adapter: 'sqlite3', filename: 'filename' })).toEqual({
      adapter: 'sqlite3',
      executeQuery: expect.any(Function),
      queryToStatement: expect.any(Function),
    });
  });
  test('should return environment when adapter is "mock"', () => {
    const mockImplementation = () => Promise.resolve();
    expect(
      createEnvironment({
        adapter: 'mock',
        mockImplementation,
      })
    ).toEqual({
      adapter: 'mock',
      executeQuery: expect.any(Function),
      queryToStatement: expect.any(Function),
    });
  });
});
