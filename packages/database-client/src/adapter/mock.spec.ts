/* eslint-disable promise/prefer-await-to-callbacks */
import { SQL } from '..';
import { MockEnvironment } from './mock';

describe('MockEnvironment()', () => {
  const name = 'Toto';
  const anyStatement = SQL`SELECT author FROM books WHERE name=${name}`;
  const anyEnvironment = MockEnvironment({});

  describe('.adapter', () => {
    test('should be "mock"', () => {
      expect(anyEnvironment.adapter).toEqual('mock');
    });
  });

  describe('.executeQuery()', () => {
    test('should use mockImplementation function', async () => {
      const mockImplementation = jest.fn(() => Promise.resolve('testReturn'));
      const environment = MockEnvironment({ mockImplementation });
      const returnValue = await environment.executeQuery(anyStatement);

      expect(mockImplementation).toHaveBeenLastCalledWith(anyStatement);
      expect(returnValue).toEqual('testReturn');
    });
  });
});
