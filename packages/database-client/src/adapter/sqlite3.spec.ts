/* eslint-disable promise/prefer-await-to-callbacks */
import { Database } from 'sqlite3';
import { SQL } from '..';
import { SQLite3Environment } from './sqlite3';

jest.mock('sqlite3', () => ({
  Database: (() => {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    function MockDatabase() {
      return undefined;
    }
    MockDatabase.prototype = {
      all: jest.fn((sql, values, callback) => callback(null, null)),
      close: jest.fn(),
    };

    return MockDatabase;
  })(),
}));

describe('sqlite3.SQLite3Environment()', () => {
  const anyStatement = SQL`SELECT ${42}`;
  const anyEnvironment = SQLite3Environment({
    filename: ':memory:',
  });

  describe('.adapter', () => {
    test('should be "sqlite3"', () => {
      expect(SQLite3Environment({ filename: '' }).adapter).toEqual('sqlite3');
    });
  });

  describe('.executeQuery()', () => {
    test('should send query to Database', async () => {
      await anyEnvironment.executeQuery(anyStatement);

      expect(Database.prototype.all).toHaveBeenLastCalledWith(
        anyStatement.sql,
        anyStatement.values,
        expect.any(Function)
      );
    });

    test('should close connection', async () => {
      await anyEnvironment.executeQuery(anyStatement);
      expect(Database.prototype.close).toHaveBeenCalled();
    });

    test('should close connection when callback error', async () => {
      jest
        .spyOn(Database.prototype, 'all')
        .mockImplementation((sql, values, callback) => callback(new Error('MockError')));

      await expect(anyEnvironment.executeQuery(SQL`SELECT error FROM unknown_wrong_table;`)).rejects.toThrow();
      expect(Database.prototype.close).toHaveBeenCalled();
    });
  });
});
