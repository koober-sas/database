import { Result } from '@koober/std/lib/result';
import { executeQuery } from './execute';
import { Environment } from './environment';
import { SQL } from './sql';
import { SQLQuery } from './query';

describe('executeQuery()', () => {
  const anyQuery = SQL`SELECT id from table`;
  const createEnvironment = (): jest.Mocked<Environment> => ({
    adapter: 'mock',
    executeQuery: jest.fn(),
    queryToStatement: jest.fn(SQLQuery.toSQLStatement),
  });

  test('should forward query execution to environment', () => {
    const environment = createEnvironment();
    executeQuery(environment, anyQuery);
    expect(environment.executeQuery).toHaveBeenCalledWith(anyQuery);
  });

  test('should return Result.Success of environment.executeQuery if promise resolved', async () => {
    const environment = createEnvironment();
    environment.executeQuery.mockReturnValue(Promise.resolve('TestReturn'));
    await expect(executeQuery(environment, anyQuery)).resolves.toEqual(Result.Success('TestReturn'));
  });

  test('should return Result.Failure of environment.executeQuery if promise rejected', async () => {
    const environment = createEnvironment();
    environment.executeQuery.mockReturnValue(Promise.reject('MockError')); // eslint-disable-line prefer-promise-reject-errors
    await expect(executeQuery(environment, anyQuery)).resolves.toEqual(Result.Failure('MockError'));
  });

  test('should convert to sql statement', () => {
    const environment = createEnvironment();
    executeQuery(environment, SQLQuery.CreateSchema({ schemaName: 'test' }));
    expect(environment.executeQuery).toHaveBeenCalledWith(SQL`CREATE SCHEMA test`);
  });
});
