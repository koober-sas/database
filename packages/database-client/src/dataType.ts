/* eslint-disable @typescript-eslint/naming-convention */
export type SQLDataType =
  | SQLDataType.CHAR
  | SQLDataType.VARCHAR
  | SQLDataType.NCHAR
  | SQLDataType.NVARCHAR
  | SQLDataType.CLOB
  | SQLDataType.BOOLEAN
  | SQLDataType.BINARY
  | SQLDataType.VARBINARY
  | SQLDataType.BLOB
  | SQLDataType.INTEGER
  | SQLDataType.SMALLINT
  | SQLDataType.BIGINT
  | SQLDataType.DECIMAL
  | SQLDataType.NUMERIC
  | SQLDataType.FLOAT
  | SQLDataType.DOUBLE_PRECISION
  | SQLDataType.REAL
  | SQLDataType.DATE
  | SQLDataType.TIME
  | SQLDataType.TIMESTAMP
  | SQLDataType.INTERVAL;
export namespace SQLDataType {
  type AnyObject = Record<string, unknown>;
  // eslint-disable-next-line @typescript-eslint/ban-types
  type EmptyObject = {};
  type DataType<T, P extends AnyObject> = Readonly<
    {
      /**
       * Data type identifier : CHAR, BINARY, etc
       */
      dataType: T;
    } & P
  >;

  function create<T, P extends AnyObject = EmptyObject>(constructor: { dataType: T }, params: P): DataType<T, P> {
    return {
      dataType: constructor.dataType,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ...params,
    };
  }

  //
  // String type
  //
  export type CHAR = DataType<typeof CHAR.dataType, { size: number }>;

  export function CHAR(size: number): CHAR {
    return create(CHAR, { size });
  }
  export namespace CHAR {
    export const dataType = 'CHAR';
    export const keys = ['size'];
  }

  /**
   * Varying character type
   */
  export type VARCHAR = DataType<typeof VARCHAR.dataType, { size: number }>;
  export function VARCHAR(size: number): VARCHAR {
    return create(VARCHAR, { size });
  }
  export namespace VARCHAR {
    export const dataType = 'VARCHAR';
    export const keys = ['size'];
  }

  /**
   * National Character type
   */
  export type NCHAR = DataType<typeof NCHAR.dataType, { size: number }>;
  export function NCHAR(size: number): NCHAR {
    return create(NCHAR, { size });
  }
  export namespace NCHAR {
    export const dataType = 'NCHAR';
    export const keys = ['size'];
  }

  /**
   * National Character Varying type
   */
  export type NVARCHAR = DataType<typeof NVARCHAR.dataType, { size: number }>;
  export function NVARCHAR(size: number): NVARCHAR {
    return create(NVARCHAR, { size });
  }
  export namespace NVARCHAR {
    export const dataType = 'NVARCHAR';
    export const keys = ['size'];
  }

  /**
   * Character Large Object type
   */
  export namespace CLOB {
    export const dataType = 'CLOB';
    export const keys = [];
  }
  export type CLOB = Readonly<{ dataType: typeof CLOB.dataType }>;

  //
  // Boolean type
  //
  export namespace BOOLEAN {
    export const dataType = 'BOOLEAN';
    export const keys = [];
  }
  export type BOOLEAN = Readonly<{ dataType: typeof BOOLEAN.dataType }>;

  //
  // Binary types
  //
  export type BINARY = DataType<typeof BINARY.dataType, { byteLength: number }>;
  export function BINARY(byteLength: number): BINARY {
    return create(BINARY, { byteLength });
  }
  export namespace BINARY {
    export const dataType = 'BINARY';
    export const keys = ['byteLength'];
  }

  export type VARBINARY = DataType<typeof VARBINARY.dataType, { byteLength: number }>;
  export function VARBINARY(byteLength: number): VARBINARY {
    return create(VARBINARY, { byteLength });
  }
  export namespace VARBINARY {
    export const dataType = 'VARBINARY';
    export const keys = ['byteLength'];
  }

  export namespace BLOB {
    export const dataType = 'BLOB';
    export const keys = [];
  }
  export type BLOB = Readonly<{ dataType: typeof BLOB.dataType }>;

  //
  // Numeric types
  //

  export namespace INTEGER {
    export const dataType = 'INTEGER';
    export const keys = [];
  }
  export type INTEGER = Readonly<{ dataType: typeof INTEGER.dataType }>;

  export namespace SMALLINT {
    export const dataType = 'SMALLINT';
    export const keys = [];
  }
  export type SMALLINT = Readonly<{ dataType: typeof SMALLINT.dataType }>;

  export namespace BIGINT {
    export const dataType = 'BIGINT';
    export const keys = [];
  }
  export type BIGINT = Readonly<{ dataType: typeof BIGINT.dataType }>;

  export type DECIMAL = DataType<typeof DECIMAL.dataType, { precision: number; scale: number }>;
  export function DECIMAL(precision: number, scale: number): DECIMAL {
    return create(DECIMAL, { precision, scale });
  }
  export namespace DECIMAL {
    export const dataType = 'DECIMAL';
    export const keys = ['precision', 'scale'];
  }

  export type NUMERIC = DataType<typeof NUMERIC.dataType, { precision: number; scale: number }>;
  export function NUMERIC(precision: number, scale: number): NUMERIC {
    return create(NUMERIC, { precision, scale });
  }
  export namespace NUMERIC {
    export const dataType = 'NUMERIC';
    export const keys = ['precision', 'scale'];
  }

  export type FLOAT = DataType<typeof FLOAT.dataType, { precision: number }>;
  export function FLOAT(precision: number): FLOAT {
    return create(FLOAT, { precision });
  }
  export namespace FLOAT {
    export const dataType = 'FLOAT';
    export const keys = ['precision'];
  }

  export namespace REAL {
    export const dataType = 'REAL';
    export const keys = [];
  }
  export type REAL = Readonly<{ dataType: typeof REAL.dataType }>;

  export namespace DOUBLE_PRECISION {
    export const dataType = 'DOUBLE PRECISION';
    export const keys = [];
  }
  export type DOUBLE_PRECISION = Readonly<{ dataType: typeof DOUBLE_PRECISION.dataType }>;

  //
  // Temporal
  //
  export namespace DATE {
    export const dataType = 'DATE';
    export const keys = [];
  }
  export type DATE = Readonly<{ dataType: typeof DATE.dataType }>;

  export namespace TIME {
    export const dataType = 'TIME';
    export const keys = [];
  }
  export type TIME = Readonly<{ dataType: typeof TIME.dataType }>;

  export namespace TIMESTAMP {
    export const dataType = 'TIMESTAMP';
    export const keys = [];
  }
  export type TIMESTAMP = Readonly<{ dataType: typeof TIMESTAMP.dataType }>;

  export namespace INTERVAL {
    export const dataType = 'INTERVAL';
    export const keys = [];
  }
  export type INTERVAL = Readonly<{ dataType: typeof INTERVAL.dataType }>;

  const Modules = {
    [BOOLEAN.dataType]: BOOLEAN,
    [BLOB.dataType]: BLOB,
    [CLOB.dataType]: CLOB,
    [INTEGER.dataType]: INTEGER,
    [SMALLINT.dataType]: SMALLINT,
    [BIGINT.dataType]: BIGINT,
    [REAL.dataType]: REAL,
    [DOUBLE_PRECISION.dataType]: DOUBLE_PRECISION,
    [DATE.dataType]: DATE,
    [TIME.dataType]: TIME,
    [TIMESTAMP.dataType]: TIMESTAMP,
    [INTERVAL.dataType]: INTERVAL,
    [CHAR.dataType]: CHAR,
    [VARCHAR.dataType]: VARCHAR,
    [NCHAR.dataType]: NCHAR,
    [NVARCHAR.dataType]: NVARCHAR,
    [FLOAT.dataType]: FLOAT,
    [DECIMAL.dataType]: DECIMAL,
    [NUMERIC.dataType]: NUMERIC,
    [BINARY.dataType]: BINARY,
    [VARBINARY.dataType]: VARBINARY,
  };

  function isEmpty(anyValue: unknown[]): anyValue is never[] {
    return anyValue.length === 0;
  }

  /**
   *
   * @param data - the data to stringify
   * @returns the string representation
   */
  export function stringify(data: SQLDataType): string {
    const keys = Modules[data.dataType].keys;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return `${data.dataType}${isEmpty(keys) ? '' : `(${keys.map((key) => (data as any)[key]).join(',')})`}`;
  }
}
