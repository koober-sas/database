/* eslint-disable no-shadow */
import { SQL, SQLStatement } from './sql';
import { SQLDataType } from './dataType';

export type SQLQuery =
  | SQLQuery.AddColumn
  | SQLQuery.AddConstraint
  | SQLQuery.CreateSchema
  | SQLQuery.CreateTable
  | SQLQuery.DropSchema
  | SQLQuery.DropTable
  | SQLQuery.RemoveConstraint
  | SQLQuery.RemoveColumn;
export namespace SQLQuery {
  export type Order = 'ASC' | 'DESC';
  export const Order = Object.freeze({
    Ascending: 'ASC',
    Descending: 'DESC',
  });

  export type OrderClause = string | [string, Order];

  type Params<P> = Omit<P, 'type'>;

  type TriggerAction = 'CASCADE' | 'RESTRICT' | 'SET DEFAULT' | 'SET NULL' | 'NO ACTION';

  type ColumnReferencesOptions = {
    /**
     * Reference to another table
     */
    tableName?: string;

    /**
     * Reference to a column
     */
    columnName?: string;
  };

  export type ColumnAttributes = {
    /**
     * Nullable column
     */
    allowNull?: boolean;

    /**
     * Default value
     */
    defaultValue?: any;

    /**
     * Column type
     */
    type: SQLDataType;

    /**
     * Unique constraint on column
     */
    unique?: boolean | string | { name: string; message: string };

    /**
     * Primary key field
     */
    primaryKey?: boolean;

    /**
     * Auto-incremented field
     */
    autoIncrement?: boolean;

    /**
     * Database comment
     */
    comment?: string;

    /**
     * An object with reference configurations
     */
    references?: ColumnReferencesOptions;

    /**
     * Trigger action when updated
     */
    onUpdate?: TriggerAction;

    /**
     * Trigger action when deleted
     */
    onDelete?: TriggerAction;

    // values?: string[];
  };
  export type TableAttributes = Record<string, ColumnAttributes>;

  export type AddColumn = Readonly<{
    type: typeof AddColumn.type;
    tableName: string;
    columnName: string;
    columnAttributes: ColumnAttributes;
  }>;
  export function AddColumn({ tableName, columnName, columnAttributes }: Params<AddColumn>): AddColumn {
    return {
      type: AddColumn.type,
      tableName,
      columnName,
      columnAttributes,
    };
  }
  export namespace AddColumn {
    export const type = 'SQLAddColumn';
  }

  export type AddConstraint = Readonly<{
    type: typeof AddConstraint.type;
    tableName: string;
    constraintName: string;
  }>;
  export function AddConstraint({ tableName, constraintName }: Params<AddConstraint>): AddConstraint {
    return {
      type: AddConstraint.type,
      tableName,
      constraintName,
    };
  }
  export namespace AddConstraint {
    export const type = 'SQLAddConstraint';
  }

  export type CreateSchema = Readonly<{
    type: typeof CreateSchema.type;
    schemaName: string;
  }>;
  export function CreateSchema({ schemaName }: Params<CreateSchema>): CreateSchema {
    return {
      type: CreateSchema.type,
      schemaName,
    };
  }
  export namespace CreateSchema {
    export const type = 'SQLCreateSchema';
  }

  export type CreateTable = Readonly<{
    type: typeof CreateTable.type;
    tableName: string;
    tableAttributes: TableAttributes;
  }>;
  export function CreateTable({ tableName, tableAttributes }: Params<CreateTable>): CreateTable {
    return {
      type: CreateTable.type,
      tableName,
      tableAttributes,
    };
  }
  export namespace CreateTable {
    export const type = 'SQLCreateTable';
  }

  export type DropSchema = Readonly<{
    type: typeof DropSchema.type;
    schemaName: string;
  }>;
  export function DropSchema({ schemaName }: Params<DropSchema>): DropSchema {
    return {
      type: DropSchema.type,
      schemaName,
    };
  }
  export namespace DropSchema {
    export const type = 'SQLDropSchema';
  }

  export type DropTable = Readonly<{
    type: typeof DropTable.type;
    tableName: string;
  }>;
  export function DropTable({ tableName }: Params<DropTable>): DropTable {
    return {
      type: DropTable.type,
      tableName,
    };
  }
  export namespace DropTable {
    export const type = 'SQLDropTable';
  }

  export type RemoveConstraint = Readonly<{
    type: typeof RemoveConstraint.type;
    tableName: string;
    constraintName: string;
  }>;
  export function RemoveConstraint({ tableName, constraintName }: Params<RemoveConstraint>): RemoveConstraint {
    return {
      type: RemoveConstraint.type,
      tableName,
      constraintName,
    };
  }
  export namespace RemoveConstraint {
    export const type = 'SQLRemoveConstraint';
  }

  export type RemoveColumn = Readonly<{
    type: typeof RemoveColumn.type;
    tableName: string;
    columnName: string;
  }>;
  export function RemoveColumn({ tableName, columnName }: Params<RemoveColumn>): RemoveColumn {
    return {
      type: RemoveColumn.type,
      tableName,
      columnName,
    };
  }
  export namespace RemoveColumn {
    export const type = 'SQLRemoveColumn';
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  function unreachableError(data: never): Error {
    return new Error('Unreachable code');
  }

  function objectEntries<V>(object: Record<string, V>): Array<[string, V]> {
    return Object.keys(object).map((propertyName) => [propertyName, object[propertyName]]);
  }

  function stringifyDataType(data: SQLDataType): string {
    return SQLDataType.stringify(data);
  }

  function stringifyColumnAttributes(columnAttributes: ColumnAttributes): string {
    return stringifyDataType(columnAttributes.type);
  }

  function stringifyTableAttributes(tableAttributes: TableAttributes): string {
    return objectEntries(tableAttributes)
      .map(([columnName, columnAttributes]) => `\n  ${columnName} ${stringifyColumnAttributes(columnAttributes)}`)
      .join(',');
  }

  function alterTable(tableName: string, operation: string): SQLStatement {
    return SQL`ALTER TABLE `.append(tableName).append(' ').append(operation);
  }

  export function toSQLStatement(query: SQLQuery): SQLStatement {
    switch (query.type) {
      case AddColumn.type:
        return alterTable(
          query.tableName,
          `ADD ${query.columnName} ${stringifyColumnAttributes(query.columnAttributes)}`
        );
      case AddConstraint.type:
        return alterTable(query.tableName, `ADD CONSTRAINT ${query.constraintName}`);
      case CreateSchema.type:
        return SQL`CREATE SCHEMA `.append(query.schemaName);
      case CreateTable.type:
        return SQL`CREATE TABLE `
          .append(query.tableName)
          .append(' (')
          .append(stringifyTableAttributes(query.tableAttributes))
          .append('\n)');
      case DropSchema.type:
        return SQL`DROP SCHEMA `.append(query.schemaName);
      case DropTable.type:
        return SQL`DROP TABLE `.append(query.tableName);
      case RemoveConstraint.type:
        return alterTable(query.tableName, `DROP CONSTRAINT ${query.constraintName}`);
      case RemoveColumn.type:
        return alterTable(query.tableName, `DROP COLUMN ${query.columnName}`);
      default: {
        throw unreachableError(query);
      }
    }
  }
}
